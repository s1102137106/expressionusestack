/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expressionusestack;

/**
 *
 * @author joenice
 */
public class Stack<T> {

    public int topIndex = 0;
    private T[] data;
    private int dataSize = 10;

    public Stack() {
        data = (T[]) new Object[dataSize];
    }

    public void Push(T data) {
        if (IsFull()) {
            ExpendDataSize();
        }
        this.data[topIndex++] = data;
    }

    public T Pop() {
        if (IsEmpty()) {
            System.out.println("資料已經空");
            return null;
        }
        return this.data[--topIndex];//先減掉再拿出來
    }

    public T Top() {
        if (IsEmpty()) {
            System.out.println("資料已經空");
            return null;
        }
        return this.data[topIndex - 1];
    }

    public boolean IsEmpty() {
        return topIndex == 0;
    }

    public boolean IsFull() {
        return topIndex == this.data.length;
    }

    public void ExpendDataSize() {
        dataSize = dataSize * 2;
        T[] newData = (T[]) new Object[dataSize];
        for (int i = 0; i < this.data.length; i++) {
            newData[i] = this.data[i];
        }
    }

}
