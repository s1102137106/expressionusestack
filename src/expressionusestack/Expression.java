/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expressionusestack;

import java.util.Arrays;

/**
 *
 * @author joenice
 */
public class Expression {

    private EXPRESSION_TYPE type;
    private String expression;

    public enum EXPRESSION_TYPE {
        INFIX, PREFIX, POSTFIX
    };

    public Expression(EXPRESSION_TYPE type, String expression) {
        this.type = type;
        this.expression = expression;
    }

    public void ConvertToPrefix() {
        switch (this.type) {
            case INFIX://INFIX Convert to Prefix       

                break;
            case POSTFIX:   //PostFix Convert to Prefix

                break;
        }
        this.type = EXPRESSION_TYPE.PREFIX;
    }

    public void ConvertToPostfix() {
        Stack priority = new Stack();
        char[] newExpression = new char[this.expression.length()];
        int index = 0;
        switch (this.type) {
            case INFIX://INFIX Convert to Postfix       
                for (char c : this.expression.toCharArray()) {
                    switch (c) {
                        case '(':
                            priority.Push(c);
                            break;
                        case ')':
                            while (true) {
                                Object popPri = priority.Pop();
                                if (popPri.equals('(')) {
                                    break;
                                }
                                newExpression[index++] = (char) popPri;
                            }
                            break;
                        default:
                            int pri = getPriority(c);
                            if (pri == -1) {//number
                                newExpression[index++] = (char) c;//print number
                                break;
                            }

                            if (priority.IsEmpty()) {
                                priority.Push(c);
                                break;
                            }

                            while (!priority.IsEmpty()) {
                                Object popPri = priority.Top();
                                if (pri > getPriority((char) popPri) || popPri == null) {//新的 > 舊的
                                  
                                    break;
                                }
                                newExpression[index++] = (char) priority.Pop();
                            }
                            priority.Push(c);
                            break;
                    }
                }
                while (!priority.IsEmpty()) {
                    newExpression[index++] = (char) priority.Pop();
                }

                break;
            case PREFIX:   //PREFIX Convert to Postfix

                break;

        }
        this.expression = Arrays.toString(newExpression);
        this.type = EXPRESSION_TYPE.POSTFIX;
    }

    public void ConvertToInfix() {
        switch (this.type) {
            case POSTFIX://POSTFIX Convert to Infix       

                break;
            case PREFIX:   //PREFIX Convert to Infix

                break;
        }
        this.type = EXPRESSION_TYPE.INFIX;
    }

    private int getPriority(char c) {
        if ((c == '+') || (c == '-')) {
            return 0;
        }
        if ((c == '*') || (c == '/')) {
            return 1;
        }
        return -1;
    }

    @Override
    public String toString() {
        return this.expression;
    }

}
