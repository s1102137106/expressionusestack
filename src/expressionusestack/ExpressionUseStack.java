/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expressionusestack;

import expressionusestack.Expression.EXPRESSION_TYPE;

/**
 *
 * @author joenice
 */
public class ExpressionUseStack {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Expression esp = new Expression(EXPRESSION_TYPE.INFIX, "a+b*d+c/d");
        esp.ConvertToPostfix();
        System.out.println(esp.toString());

    }

}
